function main(){
document.getElementById('button-1').onclick = function(){
	document.getElementById('main').style = 'display:none;'
	let tableSize = document.getElementById('inp1').value
	Sapper(tableSize)
	}

	function Sapper(tableSize){	
		let bombs = Math.floor((tableSize*tableSize)/6)
		let pureTd = (tableSize*tableSize)-bombs
		let arr = []
		for(let i = 0;i<bombs;i++){
			arr.push('bomb')
		} 
		for(let i = 0;i<pureTd;i++){
			arr.push('')
		} 
			function random(td){
				let item = arr[Math.floor(Math.random()*arr.length)]
				
				if(item == 'bomb'){
				td.className='bombed'
					arr.shift()
				}
				else{
					td.innerHTML = item
					arr.pop()
				}
			}

					//create table + td-inner
		let table = document.createElement('table')
		table.style.borderCollapse = 'collapse'
		table.id = 'table'
		table.setAttribute('cellpadding',20)
		document.body.appendChild(table);
		for(let i = 0 ; i<tableSize;i++){
			let tr = document.createElement('tr')
			table.appendChild(tr)
			for(let j = 0 ; j<tableSize;j++){
				let td = document.createElement('td')
				tr.appendChild(td)
				random(td)
				td.style.color = 'white'
			}
		}

		let clickCounter = 0;

		countBombsNearCells()
		checkFlags()
		let but = document.createElement('button')
		but.innerHTML = 'Начать игру заново'
		but.id = 'newBut'

				// prohibit clicking on the flag-cell + create button-reset
		document.getElementById('table').onclick = function(e){
				clickCounter+=1
				if(e.target.classList.contains('withNum') && !e.target.classList.contains('flag')){
					e.target.style.color = 'green'
					e.target.classList.add('active')
				}
				gameOver(e.target)
			
				if(!e.target.classList.contains('flag')){
					for(let i=0;i<tableSize;i++){
						for(let j =0;j<tableSize;j++){
					check(e.target)
						}
					}
					checkNumNearCell()
				}
				if(clickCounter === 1){
					document.body.appendChild(but)
					delete clickCounter
					document.getElementById('newBut').onclick = function(){
						let tableSize = document.getElementsByTagName('tr').length;
						document.body.innerHTML = ''
						Sapper(tableSize)
					}
				}
			}
		
							// ~isGameOver~
					function gameOver(target){
						if(target.classList.contains('red') || target.classList.contains('bombed') ){
							for(let i = 0; i<tableSize;i++){
								for(let j = 0; j<tableSize;j++){
									if(document.getElementById('table').rows[i].cells[j].classList.contains('bombed')){
										document.getElementById('table').rows[i].cells[j].style.background = 'red'
										document.getElementById('table').rows[i].cells[j].classList.add('active')
									}
									document.getElementById('table').onclick = function(){
										return false
									}
									document.getElementById('table').oncontextmenu = function(){
										return false
									}
								}
							}
						}
					}

						//field for quantity of flags / bombs

			let newSpan = document.createElement('span')
			newSpan.id = 'field'
			document.body.appendChild(newSpan)

			let fieldWithFlags = document.getElementById('field')
			fieldWithFlags.innerHTML = checkFlags() + ' / ' + bombs
			

				// to create flags

				// for(let i = 0; i<tableSize;i++){
				// 	for(let j = 0; j<tableSize;j++){
				// 		if(!document.getElementById('table').rows[i].cells[j].classList.contains('red')){

				// 		}
				// 	}
				// }



		document.getElementById('table').oncontextmenu = function (e){
				if(!e.target.classList.contains('active') && !e.target.classList.contains('red')){
					clickCounter+=1
					if(checkFlags() !== bombs){
						e.target.classList.toggle("flag")
						e.target.style.background = e.target.style.background == 'yellow' ? 'initial' : 'yellow'
						e.target.style.color = e.target.style.color == 'yellow' ? 'white' : 'yellow'
					}
					else{
						e.target.classList.remove("flag")
					}
					checkFlags()
					fieldWithFlags.innerHTML = checkFlags() + ' / ' + bombs
				}
				if(clickCounter === 1){
					document.body.appendChild(but)
					delete clickCounter
					document.getElementById('newBut').onclick = function(){
						document.body.innerHTML = ''
						Sapper()
					}
				}
				return false;
				}
				
				






				// To count quantity of flags
		function checkFlags(){
		 	let flagCounter = 0
			for(let i = 0;i<tableSize;i++){
				for(let j = 0;j<tableSize;j++){
					if(document.getElementById('table').rows[i].cells[j].classList.contains('flag')){
						flagCounter+=1;
					}
				}
			}
			return flagCounter;
		}


		
		//To merge pure cells (5 functions)

		function check(target){
			if(target.classList.contains('td')){
				// for(let i=0;i<tableSize;i++){
				// 	for(let j =0;j<tableSize;j++){
						upLvl(target)
						downLvl(target)
						next(target)	
						prev(target)
				// 	}
				// }
					
			}
		}

		// To check previous row

		function upLvl(target){
			let loc = target.cellIndex
			if(target.classList.contains('td')){
				if(target.parentNode.previousSibling){
					if(target.parentNode.previousSibling.cells[loc]){
						if(target.parentNode.previousSibling.cells[loc].classList.contains('td') && !target.parentNode.previousSibling.cells[loc].classList.contains('flag')){
							target.classList.add('active')
							target.style.border = 0 + 'px'
							target = target.parentNode.previousSibling.cells[loc]
							if(target.classList.contains('td') && !target.classList.contains('flag')){
								target.style.border = 0 + 'px'
								target.classList.add('active')
								upLvl(target)
							}
							if(target.nextSibling){
								next(target)
							}
							if(target.previousSibling){
								prev(target)
							}
							return target
						}	
					}
				}
			}
		}

		// To check next row
		function downLvl(target){
			let loc = target.cellIndex
			if(target.classList.contains('td')){
				if(target.parentNode.nextSibling){
					if(target.parentNode.nextSibling.cells[loc]){
						if(target.parentNode.nextSibling.cells[loc].classList.contains('td') && !target.parentNode.nextSibling.cells[loc].classList.contains('flag')){
							target.classList.add('active')
							target.style.border = 0 + 'px'
							target = target.parentNode.nextSibling.cells[loc]
							if(target.classList.contains('td') && !target.classList.contains('flag')){
								target.style.border = 0 + 'px'
								target.classList.add('active')
								downLvl(target)
							}
							if(target.nextSibling){
								next(target)
							}
							if(target.previousSibling){
								prev(target)
							}
							return target
						}	
					}
				}
			}
		}	

		// To check next cell

		function next(target){
			let loc = target.cellIndex
			if(target.classList.contains('td')){
				target.style.border = 0 + 'px'
				if(target.nextSibling){
					if(target.nextSibling.classList.contains('td') && !target.nextSibling.classList.contains('flag')){
						target.classList.add('active')
						target.style.border = 0 + 'px'
						target = target.nextSibling
						next(target)
					}
					return target
				}
				if(target.parentNode.previousSibling){
					upLvl(target)
				}
				if(target.parentNode.nextSibling){
					downLvl(target)
				}
			}
		}

		// To check previous cell
		function prev(target){
			let loc = target.cellIndex
			if(target.classList.contains('td')){
				target.style.border = 0 + 'px'
				if(target.previousSibling){
					if(target.previousSibling.classList.contains('td') && !target.previousSibling.classList.contains('flag')){
						target.style.border = 0 + 'px'
						target.classList.add('active')
						target = target.previousSibling
						prev(target)
					}
					return target
				}
				if(target.parentNode.previousSibling){
					upLvl(target)
				}
				if(target.parentNode.nextSibling){
					downLvl(target)
				}
			}
		}

			//Check numbers near pure cells

		function checkNumNearCell(){
			for(let i = 0 ; i<tableSize;i++){
				for(let j = 0; j<tableSize;j++){
					if(document.getElementById('table').rows[i].cells[j]){
						if(document.getElementById('table').rows[i].cells[j].style.border === 0 + 'px'){
							if(document.getElementById('table').rows[i+1]){
								if(document.getElementById('table').rows[i+1].cells[j]){
									if(document.getElementById('table').rows[i+1].cells[j].classList.contains('withNum')){
										document.getElementById('table').rows[i+1].cells[j].style.color = 'green'
										document.getElementById('table').rows[i+1].cells[j].classList.add('active')
									}
								}
							}
							if(document.getElementById('table').rows[i+1]){
								if(document.getElementById('table').rows[i+1].cells[j+1]){
									if(document.getElementById('table').rows[i+1].cells[j+1].classList.contains('withNum')){
										document.getElementById('table').rows[i+1].cells[j+1].style.color = 'green'
										document.getElementById('table').rows[i+1].cells[j+1].classList.add('active')
									}
								}
							}
							if(document.getElementById('table').rows[i]){
								if(document.getElementById('table').rows[i].cells[j+1]){
									if(document.getElementById('table').rows[i].cells[j+1].classList.contains('withNum')){
										document.getElementById('table').rows[i].cells[j+1].style.color = 'green'
										document.getElementById('table').rows[i].cells[j+1].classList.add('active')
									}
								}
							}
							if(document.getElementById('table').rows[i-1]){
								if(document.getElementById('table').rows[i-1].cells[j+1]){
									if(document.getElementById('table').rows[i-1].cells[j+1].classList.contains('withNum')){
										document.getElementById('table').rows[i-1].cells[j+1].style.color = 'green'
										document.getElementById('table').rows[i-1].cells[j+1].classList.add('active')

									}
								}
							}
							if(document.getElementById('table').rows[i-1]){
								if(document.getElementById('table').rows[i-1].cells[j]){
									if(document.getElementById('table').rows[i-1].cells[j].classList.contains('withNum')){
										document.getElementById('table').rows[i-1].cells[j].style.color = 'green'
										document.getElementById('table').rows[i-1].cells[j].classList.add('active')
									}
								}
							}
							if(document.getElementById('table').rows[i-1]){
								if(document.getElementById('table').rows[i-1].cells[j-1]){
									if(document.getElementById('table').rows[i-1].cells[j-1].classList.contains('withNum')){
										document.getElementById('table').rows[i-1].cells[j-1].style.color = 'green'
										document.getElementById('table').rows[i-1].cells[j-1].classList.add('active')
									}
								}
							}
							if(document.getElementById('table').rows[i]){
								if(document.getElementById('table').rows[i].cells[j-1]){
									if(document.getElementById('table').rows[i].cells[j-1].classList.contains('withNum')){
									document.getElementById('table').rows[i].cells[j-1].style.color = 'green'
									document.getElementById('table').rows[i].cells[j-1].classList.add('active')
									}
								}
							}
							if(document.getElementById('table').rows[i+1]){
								if(document.getElementById('table').rows[i+1].cells[j-1]){
									if(document.getElementById('table').rows[i+1].cells[j-1].classList.contains('withNum')){
										document.getElementById('table').rows[i+1].cells[j-1].style.color = 'green'
										document.getElementById('table').rows[i+1].cells[j-1].classList.add('active')
									}
								}
							}
						}
					}
				}
			}
		}



			// To count bombs near cells
		function countBombsNearCells(){
			for(let i = 0 ; i<tableSize;i++){
				for(let j = 0; j<tableSize;j++){
					if(document.getElementById('table').rows[i].cells[j]){
						if(document.getElementById('table').rows[i].cells[j].className !== 'bombed'){
							let counter = 0;
							if(document.getElementById('table').rows[i+1]){
								if(document.getElementById('table').rows[i+1].cells[j]){
									if(document.getElementById('table').rows[i+1].cells[j].className == 'bombed'){
									counter++
									}
								}
							}
							if(document.getElementById('table').rows[i+1]){
								if(document.getElementById('table').rows[i+1].cells[j+1]){
									if(document.getElementById('table').rows[i+1].cells[j+1].className == 'bombed'){
										counter++
									}
								}
							}
							if(document.getElementById('table').rows[i]){
								if(document.getElementById('table').rows[i].cells[j+1]){
									if(document.getElementById('table').rows[i].cells[j+1].className == 'bombed'){
										counter++
									}
								}
							}
							if(document.getElementById('table').rows[i-1]){
								if(document.getElementById('table').rows[i-1].cells[j+1]){
									if(document.getElementById('table').rows[i-1].cells[j+1].className == 'bombed'){
										counter++
									}
								}
							}
							if(document.getElementById('table').rows[i-1]){
								if(document.getElementById('table').rows[i-1].cells[j]){
									if(document.getElementById('table').rows[i-1].cells[j].className == 'bombed'){
										counter++
									}
								}
							}
							if(document.getElementById('table').rows[i-1]){
								if(document.getElementById('table').rows[i-1].cells[j-1]){
									if(document.getElementById('table').rows[i-1].cells[j-1].className == 'bombed'){
										counter++
									}
								}
							}
							if(document.getElementById('table').rows[i]){
								if(document.getElementById('table').rows[i].cells[j-1]){
									if(document.getElementById('table').rows[i].cells[j-1].className == 'bombed'){
									counter++
									}
								}
							}
							if(document.getElementById('table').rows[i+1]){
								if(document.getElementById('table').rows[i+1].cells[j-1]){
									if(document.getElementById('table').rows[i+1].cells[j-1].className == 'bombed'){
										counter++
									}
								}
							}
							if(counter > 0){
								document.getElementById('table').rows[i].cells[j].className = 'withNum'
								document.getElementById('table').rows[i].cells[j].innerHTML = counter
							}
							else{
								document.getElementById('table').rows[i].cells[j].className = 'td'
							}
						}
					}
				}
			}
		}

			// doubleclick with flag
		document.getElementById('table').ondblclick = function(e){
			for(let i = 0 ; i<tableSize;i++){
				for(let j = 0; j<tableSize;j++){
					if(document.getElementById('table').rows[i].cells[j] == e.target){
						if(document.getElementById('table').rows[i].cells[j].classList.contains('withNum') && document.getElementById('table').rows[i].cells[j].classList.contains('active')){
							let counter = 0;
							if(document.getElementById('table').rows[i+1]){
								if(document.getElementById('table').rows[i+1].cells[j]){
									if(document.getElementById('table').rows[i+1].cells[j].classList.contains('flag')){
									counter++
									}
								}
							}
							if(document.getElementById('table').rows[i+1]){
								if(document.getElementById('table').rows[i+1].cells[j+1]){
									if(document.getElementById('table').rows[i+1].cells[j+1].classList.contains('flag')){
										counter++
									}
								}
							}
							if(document.getElementById('table').rows[i]){
								if(document.getElementById('table').rows[i].cells[j+1]){
									if(document.getElementById('table').rows[i].cells[j+1].classList.contains('flag')){
										counter++
									}
								}
							}
							if(document.getElementById('table').rows[i-1]){
								if(document.getElementById('table').rows[i-1].cells[j+1]){
									if(document.getElementById('table').rows[i-1].cells[j+1].classList.contains('flag')){
										counter++
									}
								}
							}
							if(document.getElementById('table').rows[i-1]){
								if(document.getElementById('table').rows[i-1].cells[j]){
									if(document.getElementById('table').rows[i-1].cells[j].classList.contains('flag')){
										counter++
									}
								}
							}
							if(document.getElementById('table').rows[i-1]){
								if(document.getElementById('table').rows[i-1].cells[j-1]){
									if(document.getElementById('table').rows[i-1].cells[j-1].classList.contains('flag')){
										counter++
									}
								}
							}
							if(document.getElementById('table').rows[i]){
								if(document.getElementById('table').rows[i].cells[j-1]){
									if(document.getElementById('table').rows[i].cells[j-1].classList.contains('flag')){
										counter++
									}
								}
							}
							if(document.getElementById('table').rows[i+1]){
								if(document.getElementById('table').rows[i+1].cells[j-1]){
									if(document.getElementById('table').rows[i+1].cells[j-1].classList.contains('flag')){
										counter++
									}
								}
							}
							if(counter == e.target.innerHTML){
								if(document.getElementById('table').rows[i].cells[j] == e.target){
									if(document.getElementById('table').rows[i+1]){
										if(document.getElementById('table').rows[i+1].cells[j]){
											if(!document.getElementById('table').rows[i+1].cells[j].classList.contains('flag')){
												if(document.getElementById('table').rows[i+1].cells[j].classList.contains('td')){
													document.getElementById('table').rows[i+1].cells[j].style.border = 0 + 'px'
												}
												if(document.getElementById('table').rows[i+1].cells[j].classList.contains('withNum') && !document.getElementById('table').rows[i].cells[j].classList.contains('flag')){
													document.getElementById('table').rows[i+1].cells[j].style.color = 'green'
												}
												if(document.getElementById('table').rows[i+1].cells[j].classList.contains('bombed')){
													document.getElementById('table').rows[i+1].cells[j].classList.add('red')
													gameOver(document.getElementById('table').rows[i+1].cells[j])
												}
												document.getElementById('table').rows[i+1].cells[j].classList.add('active')
											}
										}
									}
									if(document.getElementById('table').rows[i+1]){
										if(document.getElementById('table').rows[i+1].cells[j+1]){
											if(!document.getElementById('table').rows[i+1].cells[j+1].classList.contains('flag')){
											document.getElementById('table').rows[i+1].cells[j+1].style.border = 0 + 'px'
												document.getElementById('table').rows[i+1].cells[j+1].classList.add('active')
												if(document.getElementById('table').rows[i+1].cells[j+1].classList.contains('td')){
													document.getElementById('table').rows[i+1].cells[j+1].style.border = 0 + 'px'
												}
												if(document.getElementById('table').rows[i+1].cells[j+1].classList.contains('withNum') && !document.getElementById('table').rows[i].cells[j].classList.contains('flag')){
													document.getElementById('table').rows[i+1].cells[j+1].style.color = 'green'
												}
												if(document.getElementById('table').rows[i+1].cells[j+1].classList.contains('bombed')){
													document.getElementById('table').rows[i+1].cells[j+1].classList.add('red')
													gameOver(document.getElementById('table').rows[i+1].cells[j+1])
												}
											}
										}
												
										}
									}
									if(document.getElementById('table').rows[i]){
										if(document.getElementById('table').rows[i].cells[j+1]){
											if(!document.getElementById('table').rows[i].cells[j+1].classList.contains('flag')){
												if(document.getElementById('table').rows[i].cells[j+1].classList.contains('td')){
													document.getElementById('table').rows[i].cells[j+1].style.border = 0 + 'px'
												}
												if(document.getElementById('table').rows[i].cells[j+1].classList.contains('withNum') && !document.getElementById('table').rows[i].cells[j].classList.contains('flag')){
													document.getElementById('table').rows[i].cells[j+1].style.color = 'green'
												}
												if(document.getElementById('table').rows[i].cells[j+1].classList.contains('bombed')){
													document.getElementById('table').rows[i].cells[j+1].classList.add('red')
													gameOver(document.getElementById('table').rows[i].cells[j+1])
												}
												document.getElementById('table').rows[i].cells[j+1].classList.add('active')
											}	
										}
									}
									if(document.getElementById('table').rows[i-1]){
										if(document.getElementById('table').rows[i-1].cells[j+1]){
											if(!document.getElementById('table').rows[i-1].cells[j+1].classList.contains('flag')){
												if(document.getElementById('table').rows[i-1].cells[j+1].classList.contains('td')){
													document.getElementById('table').rows[i-1].cells[j+1].style.border = 0 + 'px'
												}
												if(document.getElementById('table').rows[i-1].cells[j+1].classList.contains('withNum') && !document.getElementById('table').rows[i].cells[j].classList.contains('flag')){
													document.getElementById('table').rows[i-1].cells[j+1].style.color = 'green'
												}
												if(document.getElementById('table').rows[i-1].cells[j+1].classList.contains('bombed')){
													document.getElementById('table').rows[i-1].cells[j+1].classList.add('red')
													gameOver(document.getElementById('table').rows[i-1].cells[j+1])
												}
												document.getElementById('table').rows[i-1].cells[j+1].classList.add('active')
											}
										}
									}
									if(document.getElementById('table').rows[i-1]){
										if(document.getElementById('table').rows[i-1].cells[j]){
											if(!document.getElementById('table').rows[i-1].cells[j].classList.contains('flag')){
												if(document.getElementById('table').rows[i-1].cells[j].classList.contains('td')){
													document.getElementById('table').rows[i-1].cells[j].style.border = 0 + 'px'
												}
												if(document.getElementById('table').rows[i-1].cells[j].classList.contains('withNum') && !document.getElementById('table').rows[i].cells[j].classList.contains('flag')){
													document.getElementById('table').rows[i-1].cells[j].style.color = 'green'
												}
												if(document.getElementById('table').rows[i-1].cells[j].classList.contains('bombed')){
													document.getElementById('table').rows[i-1].cells[j].classList.add('red')
													gameOver(document.getElementById('table').rows[i-1].cells[j])
												}
												document.getElementById('table').rows[i-1].cells[j].classList.add('active')
											}
										}
									}
									if(document.getElementById('table').rows[i-1]){
										if(document.getElementById('table').rows[i-1].cells[j-1]){
											if(!document.getElementById('table').rows[i-1].cells[j-1].classList.contains('flag')){
												if(document.getElementById('table').rows[i-1].cells[j-1].classList.contains('td')){
													document.getElementById('table').rows[i-1].cells[j-1].style.border = 0 + 'px'
												}
												if(document.getElementById('table').rows[i-1].cells[j-1].classList.contains('withNum') && !document.getElementById('table').rows[i].cells[j].classList.contains('flag')){
													document.getElementById('table').rows[i-1].cells[j-1].style.color = 'green'
												}
												if(document.getElementById('table').rows[i-1].cells[j-1].classList.contains('bombed')){
													document.getElementById('table').rows[i-1].cells[j-1].classList.add('red')
													gameOver(document.getElementById('table').rows[i-1].cells[j-1])
												}
												document.getElementById('table').rows[i-1].cells[j-1].classList.add('active')
											}
										}
									}
									if(document.getElementById('table').rows[i]){
										if(document.getElementById('table').rows[i].cells[j-1]){
											if(!document.getElementById('table').rows[i].cells[j-1].classList.contains('flag')){
												if(document.getElementById('table').rows[i].cells[j-1].classList.contains('td')){
													document.getElementById('table').rows[i].cells[j-1].style.border = 0 + 'px'
												}
												if(document.getElementById('table').rows[i].cells[j-1].classList.contains('withNum') && !document.getElementById('table').rows[i].cells[j].classList.contains('flag')){
													document.getElementById('table').rows[i].cells[j-1].style.color = 'green'
												}
												if(document.getElementById('table').rows[i].cells[j-1].classList.contains('bombed')){
													document.getElementById('table').rows[i].cells[j-1].classList.add('red')
													gameOver(document.getElementById('table').rows[i].cells[j-1])
												}
											document.getElementById('table').rows[i].cells[j-1].classList.add('active')
											}	
										}
									}
									if(document.getElementById('table').rows[i+1]){
										if(document.getElementById('table').rows[i+1].cells[j-1]){
											if(!document.getElementById('table').rows[i+1].cells[j-1].classList.contains('flag')){
												if(document.getElementById('table').rows[i+1].cells[j-1].classList.contains('td')){
													document.getElementById('table').rows[i+1].cells[j-1].style.border = 0 + 'px'
												}
												if(document.getElementById('table').rows[i+1].cells[j-1].classList.contains('withNum') && !document.getElementById('table').rows[i].cells[j].classList.contains('flag')){
													document.getElementById('table').rows[i+1].cells[j-1].style.color = 'green'
												}
												if(document.getElementById('table').rows[i+1].cells[j-1].classList.contains('bombed')){
													document.getElementById('table').rows[i+1].cells[j-1].classList.add('red')
													gameOver(document.getElementById('table').rows[i+1].cells[j-1])
												}
												document.getElementById('table').rows[i+1].cells[j-1].classList.add('active')
											}	
										}
									}					
								}
							}
						}			
		}
	}
}
}

}
main()